﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TataPower.MDM.Domain.Data
{
    interface IDbConnections:IDisposable
    {
        bool IsDisposed { get; }
    }
}
